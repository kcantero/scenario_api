/*******************************************************************************
 * Copyright (c) 2021-2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  i_lane_location_query_service.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_MAP_ILANELOCATIONQUERYSERVICE_H
#define MANTLEAPI_MAP_ILANELOCATIONQUERYSERVICE_H

#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/pose.h>
#include <MantleAPI/Common/vector.h>
#include <units.h>

#include <optional>
#include <vector>

namespace mantle_api
{
enum class Direction
{
  kForward = 0,
  kBackwards = 1
};

/// Abstraction layer for all map related functions.
class ILaneLocationQueryService
{
public:
  virtual ~ILaneLocationQueryService() = default;

  /// TODO: Currently LaneLocationProvider just inherits from this for the sake of easily passing it to the
  /// controllers
  /// Therefore the GetMapObjectById function has been removed from the interface - for now.
  /// We need to think about proper interface functions we want to define here (GetLaneLocation? GetLanes? But this
  /// would add a rat-tail of more interfaces we need to define (ILaneLocation, ILane, ...?)
  // virtual const IIdentifiable& GetMapObjectById(UniqueId id) = 0;

  virtual Orientation3<units::angle::radian_t> GetLaneOrientation(
      const Vec3<units::length::meter_t>& position) const = 0;
  virtual Vec3<units::length::meter_t> GetUpwardsShiftedLanePosition(const Vec3<units::length::meter_t>& position,
                                                                     double upwards_shift,
                                                                     bool allow_invalid_positions = false) const = 0;
  virtual bool IsPositionOnLane(const Vec3<units::length::meter_t>& position) const = 0;
  virtual std::vector<UniqueId> GetLaneIdsAtPosition(const Vec3<units::length::meter_t>& position) const = 0;

  /// @brief Calculate the new pose which is at certain distance from the reference_pose_on_lane following a direction.
  ///
  ///@param reference_pose_on_lane  Starting position. Must be on a lane.
  ///@param direction  Direction to go along the lane from the reference_pose_on_lane. The orientation of the reference pose is used to determine the forward direction. "Forward" means aligned with the positive x direction of the local coordinate system defined by the pose.
  ///@return Pose that is at the given distance from reference_pose_on_lane in the specified direction. The new orientation is parallel to the lane orientation at the target position.
  virtual std::optional<Pose> FindLanePoseAtDistanceFrom(const Pose& reference_pose_on_lane, units::length::meter_t distance, Direction direction) const = 0;

  /// @brief Calculate a new pose which is at certain distance in a relative lane from the reference_pose_on_lane.
  ///
  ///@param reference_pose_on_lane  Starting position. Must be on a lane.
  ///@param relative_target_lane  Shift of the target position in number of lanes relative to the lane where the reference pose is located. Positive to the left, negative to the right.
  ///@param distance  The longitudinal distance along the centerline of the lane closest to the reference pose. The orientation of the reference pose is used to determine the direction. Positive distance means aligned with the positive x direction of the local coordinate system defined by the pose.
  ///@param lateral_offset  Lateral offset to the target lane
  ///@return Pose that is at the given distance from reference_pose_on_lane in a relative lane. The new orientation is parallel to the lane orientation at the target position.
  virtual std::optional<Pose> FindRelativeLanePoseAtDistanceFrom(const Pose& reference_pose_on_lane, int relative_target_lane, units::length::meter_t distance, units::length::meter_t lateral_offset) const = 0;

  /// @brief Calculate the lane id of the relative target lane from a given position
  ///
  ///@param reference_pose_on_lane  Starting position. Must be on a lane.
  ///@param relative_target_lane  Shift of the target position in number of lanes relative to the lane where the reference pose is located. Positive to the left, negative to the right.
  ///@return Lane id that is at the given lateral shift (relative_lane_target) from given position (reference_pose_on_lane).
  ///        No value, if reference pose is not on a lane or if the lane doesn't have a suitable adjacent lane
  virtual std::optional<mantle_api::UniqueId> GetRelativeLaneId(const mantle_api::Pose& reference_pose_on_lane, int relative_lane_target) const = 0;
};

}  // namespace mantle_api

#endif  // MANTLEAPI_MAP_ILANELOCATIONQUERYSERVICE_H
