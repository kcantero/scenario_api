/*******************************************************************************
 * Copyright (c) 2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  i_geometry_helper.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_COMMON_IGEOMETRYHELPER_H
#define MANTLEAPI_COMMON_IGEOMETRYHELPER_H

#include <MantleAPI/Common/pose.h>

#include <vector>

namespace mantle_api
{
/// Interface that provides functionality to perform geometrical calculations
class IGeometryHelper
{
public:
  virtual ~IGeometryHelper() = default;

  /// @brief Translates a given global position in a local coordinate system
  ///        and returns the new global position.
  /// @param global_position   starting position in global coordinates
  /// @param local_orientation orientation of local coordinate system compared to
  ///                          the global coordinate system
  /// @param local_translation desired translation in local coordinates
  /// @return translated position in global coordinates
  virtual Vec3<units::length::meter_t> TranslateGlobalPositionLocally(
      const Vec3<units::length::meter_t>& global_position,
      const Orientation3<units::angle::radian_t>& local_orientation,
      const Vec3<units::length::meter_t>& local_translation) const = 0;

  /// @brief Transforms world polyline positions to local coordinate system.
  /// @param polyline_points world polyline points to be transformed
  /// @param local_origin  local coordinate system origin
  /// @param local_orientation local system orientation
  /// @return converted polyline points
  virtual std::vector<Vec3<units::length::meter_t>> TransformPolylinePointsFromWorldToLocal(
      const std::vector<Vec3<units::length::meter_t>>& polyline_points,
      const Vec3<units::length::meter_t>& local_origin,
      const Orientation3<units::angle::radian_t>& local_orientation) const = 0;

  /// @brief Transforms world position to local coordinate system.
  /// @param world_position world position to be transformed
  /// @param local_origin  local coordinate system origin
  /// @param local_orientation local system orientation
  /// @return transformed point
  virtual Vec3<units::length::meter_t> TransformPositionFromWorldToLocal(
      const Vec3<units::length::meter_t>& world_position,
      const Vec3<units::length::meter_t>& local_origin,
      const Orientation3<units::angle::radian_t>& local_orientation) const = 0;
};

}  // namespace mantle_api

#endif  // MANTLEAPI_COMMON_IGEOMETRYHELPER_H
